/*
 *
 * Hayden Coffey and Michael Jantz
 * 2020
 *
 * membench is a memory access benchmark. The workload accesses data allocated in
 * two distinct arrays, repeatedly, and in parallel, using a specified number of
 * threads.
 *
 * membench creates two arrays (hot and cold) with data elements. Each data
 * element is an array of some (configurable) number of bytes initialized with
 * random data. membench also creates a third array, called the address array,
 * which stores addresses to a subset of elements in the hot and cold arrays.
 * A command line option (cold_pct or -p) controls the portion of data elements
 * that are selected from the cold array, and all other elements are selected
 * from the hot array.
 *
 * After initialization, membench shuffles the address array and divides it
 * evenly amongst a number of threads. The threads then continually traverse
 * their own portions of the address array in parallel and for some specified
 * amount of time. During this traversal, each thread sequentially accesses
 * the bytes in each data element in its portion of the address array with the
 * specified access pattern (read-only or read/write). The benchmark reports
 * throughput in both number of data elements accessed per second and GBs
 * accessed per second.
 *
 */

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <unistd.h>
#include <time.h> 
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include <signal.h>
#include <sys/time.h>   
#include <sys/resource.h> 

#include <sicm_low.h>

#define MIN_ELEM_SIZE 3
#define MAX_ELEM_SIZE 63
#define MIN_ELEMS 0
#define MAX_ELEMS 63
#define MIN_NR_THREADS 1
#define MAX_NR_THREADS sysconf(_SC_NPROCESSORS_ONLN)
#define BYTES_ACCESSED_TIMER (1ull<<30)

const char usage[] =
"Usage: membench [OPTIONS]\n"
"membench: memory access benchmark. Access data allocated in two distinct\n"
"arrays, repeatedly, using a specified number of threads.\n\n"
"Arguments and options\n\n"
"--elem_size,-E     Required, size of elements in data arrays as power of 2\n"
"--cold_elems,-C    Required, size of cold array as power of 2\n"
"--hot_elems,-H     Required, size of hot array as power of 2\n"
"--addr_elems,-A    Size of address array as power of 2, defaults to min size of hot/cold arrays\n"
"--threads,-t       Number of threads accessing the data elements, defaults to 1\n"
"--cold_pct,-p      Percent chance of selecting an address from the cold array, defaults to 50.0\n"
"--pattern,-r       Access pattern, read-only : 'r' or read-write : 'w', defaults to read-only\n"
"--seed,-s          Value to seed random number generator with, defaults to 0\n"
"--exe_time,-x      Time in seconds for threads to traverse the address array, defaults to 20\n"
"--default_dev,-d   Default numa node for heap data, defaults to 0\n"
"--hot_dev,-m       NUMA node for hot array, defaults to default_dev\n"
"--cold_dev,-n      NUMA node for cold array, defaults to default_dev\n"
"\n"
"Restrictions\n"
"elem_size must be between 3 and 63\n"
"cold_elems and hot_elems must between 0 and 63\n"
"addr_elems must be at least log2(threads), and cannot be larger than cold_elems or hot_elems\n"
"threads must be between 1 and sysconf(_SC_NPROCESSORS_ONLN)\n"
"cold_pct must be between 0 and 100\n"
"\n"
;

struct thread_data_t {
  size_t start_idx, nr_elems, elem_size;
  char **addr_arr;
  unsigned long long nr_accs, sum;
  unsigned int exe_time;
};

double get_pct_arg(char *arg)
{
  double ret;

  ret = atof(arg);
  if ( (ret < 0.0) || (ret > 100.0) ) {
    printf(usage);
    exit(-EINVAL);
  }

  return ret;
}

ssize_t get_size_arg(char *arg, ssize_t min, ssize_t max)
{
  ssize_t size;

  size = atoi(arg);
  if ( (size < min) || (size > max) ) {
    printf(usage);
    return -EINVAL;
  }

  return size;
}

sicm_arena create_numa_sicm_arena(unsigned int numa_id, sicm_device_list devs)
{
  size_t i;
  int dev_idx;
  sicm_device_list ds;
  sicm_arena s;

  dev_idx = -1;
  for (i = 0; i < devs.count; i++) {
    if ((int)numa_id == sicm_numa_id(devs.devices[i])) {
      dev_idx = i;
      break;
    }
  }

  if (dev_idx == -1) {
    printf("error: invalid numa id for sicm device: %u\n", numa_id);
    printf(usage);
    exit(-EINVAL);
  }

  ds.count = 1;
  ds.devices = &devs.devices[dev_idx];
	s = sicm_arena_create(0, SICM_ALLOC_RELAXED, &ds);
	if (s == NULL) {
		fprintf(stderr, "sicm_arena_create failed\n");
    exit(-EINVAL);
	}

  return s;
}

void parse_options(int argc, char **argv, ssize_t *elem_log2_size,
  ssize_t *cold_log2_elems, ssize_t *hot_log2_elems, ssize_t *addr_log2_elems,
  ssize_t *nr_threads, double *cold_pct, char *pattern,
  unsigned int *seed, unsigned int *exe_time, unsigned int *default_device,
  unsigned int *hot_device, unsigned int *cold_device) {

  int c;
  unsigned int elem_log2_set, cold_log2_set, hot_log2_set, addr_log2_set;
  unsigned int hot_device_set, cold_device_set;

  elem_log2_set = cold_log2_set = hot_log2_set = addr_log2_set = 0;
  hot_device_set = cold_device_set = 0;
  while (1) {
    static struct option long_options[] =
      {
        /* These options set a flag. */
        {"elem_size",    required_argument, 0, 'E'},
        {"cold_elems",   required_argument, 0, 'C'},
        {"hot_elems",    required_argument, 0, 'H'},
        {"addr_elems",   required_argument, 0, 'A'},
        {"threads",      required_argument, 0, 't'},
        {"cold_pct",     required_argument, 0, 'p'},
        {"pattern",      required_argument, 0, 'r'},
        {"seed",         required_argument, 0, 's'},
        {"exe_time",     required_argument, 0, 'x'},
        {"default_dev",  required_argument, 0, 'd'},
        {"hot_dev",      required_argument, 0, 'm'},
        {"cold_dev",     required_argument, 0, 'n'},
        {0, 0, 0, 0}
      };
    /* getopt_long stores the option index here. */
    int option_index = 0;

    c = getopt_long (argc, argv, "E:C:H:A:t:p:r:s:x:d:m:n:",
                     long_options, &option_index);

    /* Detect the end of the options. */
    if (c == -1)
      break;

    switch (c) {
    case 0:
      if (strcmp(long_options[option_index].name, "elem_size") == 0) {
        *elem_log2_size = get_size_arg(optarg, MIN_ELEM_SIZE, MAX_ELEM_SIZE);
        elem_log2_set = 1;
      } else if (strcmp(long_options[option_index].name, "cold_elems") == 0) {
        *cold_log2_elems = get_size_arg(optarg, MIN_ELEMS, MAX_ELEMS);
        cold_log2_set = 1;
      } else if (strcmp(long_options[option_index].name, "hot_elems") == 0) {
        *hot_log2_elems = get_size_arg(optarg, MIN_ELEMS, MAX_ELEMS);
        hot_log2_set = 1;
      } else if (strcmp(long_options[option_index].name, "addr_elems") == 0) {
        *addr_log2_elems = get_size_arg(optarg, MIN_ELEMS, MAX_ELEMS);
        addr_log2_set = 1;
      } else if (strcmp(long_options[option_index].name, "threads") == 0) {
        *nr_threads = get_size_arg(optarg, MIN_NR_THREADS, MAX_NR_THREADS);
      } else if (strcmp(long_options[option_index].name, "cold_pct") == 0) {
        *cold_pct = get_pct_arg(optarg);
      } else if (strcmp(long_options[option_index].name, "pattern") == 0) {
        *pattern = optarg[0];
      } else if (strcmp(long_options[option_index].name, "seed") == 0) {
        *seed = strtoul(optarg,NULL,10);
      } else if (strcmp(long_options[option_index].name, "exe_time") == 0) {
        *exe_time = strtoul(optarg,NULL,10);
      } else if (strcmp(long_options[option_index].name, "default_dev") == 0) {
        *default_device = strtoul(optarg,NULL,10);
      } else if (strcmp(long_options[option_index].name, "hot_dev") == 0) {
        *hot_device = strtoul(optarg,NULL,10);
        hot_device_set = 1;
      } else if (strcmp(long_options[option_index].name, "cold_dev") == 0) {
        *cold_device = strtoul(optarg,NULL,10);
        cold_device_set = 1;
      }

    case 'E':
      *elem_log2_size = get_size_arg(optarg, MIN_ELEM_SIZE, MAX_ELEM_SIZE);
      elem_log2_set = 1;
      break;

    case 'C':
      *cold_log2_elems = get_size_arg(optarg, MIN_ELEMS, MAX_ELEMS);
      cold_log2_set = 1;
      break;

    case 'H':
      *hot_log2_elems = get_size_arg(optarg, MIN_ELEMS, MAX_ELEMS);
      hot_log2_set = 1;
      break;

    case 'A':
      *addr_log2_elems = get_size_arg(optarg, MIN_ELEMS, MAX_ELEMS);
      addr_log2_set = 1;
      break;

    case 't':
      *nr_threads = get_size_arg(optarg, MIN_NR_THREADS, MAX_NR_THREADS);
      break;

    case 'p':
      *cold_pct = get_pct_arg(optarg);
      break;

    case 'r':
      *pattern = optarg[0];
      break;

    case 's':
      *seed = strtoul(optarg,NULL,10);
      break;

    case 'x':
      *exe_time = strtoul(optarg,NULL,10);
      break;

    case 'd':
      *default_device = strtoul(optarg,NULL,10);
      break;

    case 'm':
      *hot_device = strtoul(optarg,NULL,10);
      hot_device_set = 1;
      break;

    case 'n':
      *cold_device = strtoul(optarg,NULL,10);
      cold_device_set = 1;
      break;

    case '?':
      printf(usage);
      exit(-EINVAL);

    default:
      abort ();
    }
  }

  if ((*addr_log2_elems > *cold_log2_elems) || (*addr_log2_elems > *hot_log2_elems)) {
    printf("error: addr_elems cannot be larger than cold_elems or hot_elems\n");
    printf(usage);
    exit(-EINVAL);
  }

  if (!(elem_log2_set || cold_log2_set || hot_log2_set || addr_log2_set)) {
    printf(usage);
    exit(-EINVAL);
  }

  if (!hot_device_set) {
    *hot_device = *default_device;
  }

  if (!cold_device_set) {
    *cold_device = *default_device;
  }
}

void shuffle_addr_array(char **addr_arr, unsigned long long addr_elems)
{
  size_t i, j;
  char *tmp;
  for (i = (addr_elems - 1); i > 0; i--) {
    j = (rand() % (i + 1));
    tmp = addr_arr[i];
    addr_arr[i] = addr_arr[j];
    addr_arr[j] = tmp;
  }
}

char **get_addr_array(size_t addr_elems, size_t elem_size, double cold_pct,
  char *hot_arr, char* cold_arr, unsigned long long *hot_elems,
  unsigned long long *cold_elems, sicm_arena arena)
{
  char **addr_arr;
  size_t i, hot_idx, cold_idx, thresh, val;

  addr_arr = (char **) sicm_arena_alloc(arena, (addr_elems*(sizeof(char*))));
  if (!addr_arr) {
    exit(-ENOMEM);
  }

  hot_idx = cold_idx = 0;
  thresh = (size_t) (cold_pct * 100);
  for (i = 0; i < addr_elems; i++) {
    val = rand() % 10000;
    if (val < thresh) {
      addr_arr[i] = &(cold_arr[cold_idx]);
      cold_idx += elem_size;
      (*(cold_elems))++;
    } else {
      addr_arr[i] = &(hot_arr[hot_idx]);
      hot_idx += elem_size;
      (*(hot_elems))++;
    }
  }

  return addr_arr;
}

/*
 * Returns an array of bytes initialized to some random values. 
 */
char *get_elem_array(unsigned long long nr_elems,
  unsigned long long elem_size, sicm_arena arena)
{
  char *arr;
  size_t i;
  int x;
  unsigned long long nr_bytes;

  nr_bytes = (nr_elems * elem_size);

  arr = (char *) sicm_arena_alloc(arena, nr_bytes);
  if (!arr) {
    exit(-ENOMEM);
  }

  for (i = 0; i < nr_bytes; i += sizeof(int)) {
    x = rand();
    memcpy(&(arr[i]), &x, sizeof(int));
  }

  return arr;
}

double get_elapsed_seconds(struct timespec start, struct timespec end)
{
  long elapsed_sec, elapsed_nsec;

  elapsed_sec = end.tv_sec - start.tv_sec;
  elapsed_nsec = end.tv_nsec - start.tv_nsec;
	if (start.tv_nsec > end.tv_nsec) {
		--elapsed_sec; 
		elapsed_nsec += 1000000000; 
	}
  return ( (double) elapsed_sec +
           ( (double)(elapsed_nsec) / (double) 1000000000) );
}

void *read_elems(void *tdata) {
  size_t i, j, start_idx, nr_elems, elem_size;
  char *cur_addr, **addr_arr;
  unsigned long long *sum, *nr_accs, val_accs;
  struct timespec thread_start_time, thread_cur_time;
  double elapsed_time, exe_time;

  start_idx = ((struct thread_data_t*) tdata)->start_idx;
  nr_elems = ((struct thread_data_t*) tdata)->nr_elems;
  elem_size = ((struct thread_data_t*) tdata)->elem_size;
  addr_arr = ((struct thread_data_t*) tdata)->addr_arr;
  sum = (&(((struct thread_data_t*) tdata)->sum));
  nr_accs = (&(((struct thread_data_t*) tdata)->nr_accs));
  exe_time = (double)((struct thread_data_t*) tdata)->exe_time;

  elapsed_time = 0.0;
  val_accs = 0ull;
  clock_gettime(CLOCK_REALTIME, &thread_start_time);
  while (elapsed_time < exe_time) {
    for (i = start_idx; i < (start_idx+nr_elems); i++) {
      cur_addr = addr_arr[i];
      for (j = 0; j < elem_size; j+=(sizeof(unsigned long long))) {
        *sum += (*(unsigned long long*) (((char *) cur_addr) + j) );
      }
    }
    *nr_accs += nr_elems;
    val_accs += nr_elems;
    if ((val_accs*elem_size) > BYTES_ACCESSED_TIMER) {
      clock_gettime(CLOCK_REALTIME, &thread_cur_time);
      elapsed_time = get_elapsed_seconds(thread_start_time, thread_cur_time);
      val_accs = 0ull;
    }
  }

  return NULL;
}

void *rdwr_elems(void *tdata) {
  size_t i, j, start_idx, nr_elems, elem_size;
  char *cur_addr, **addr_arr;
  unsigned long long *sum, *nr_accs, val_accs;
  struct timespec thread_start_time, thread_cur_time;
  double elapsed_time, exe_time;

  start_idx = ((struct thread_data_t*) tdata)->start_idx;
  nr_elems = ((struct thread_data_t*) tdata)->nr_elems;
  elem_size = ((struct thread_data_t*) tdata)->elem_size;
  addr_arr = ((struct thread_data_t*) tdata)->addr_arr;
  sum = (&(((struct thread_data_t*) tdata)->sum));
  nr_accs = (&(((struct thread_data_t*) tdata)->nr_accs));
  exe_time = (double)((struct thread_data_t*) tdata)->exe_time;

  elapsed_time = 0.0;
  val_accs = 0ull;
  clock_gettime(CLOCK_REALTIME, &thread_start_time);
  while (elapsed_time < exe_time) {
    for (i = start_idx; i < (start_idx+nr_elems); i++) {
      cur_addr = addr_arr[i];
      for (j = 0; j < elem_size; j+=(sizeof(unsigned long long))) {
        *sum += (*(unsigned long long*) (((char *) cur_addr) + j) );
        (*(unsigned long long*) (((char *) cur_addr) + j) ) =
          (((unsigned long long)(i+j))^(*sum));
      }
    }
    *nr_accs += nr_elems;
    val_accs += *nr_accs;
    if ((val_accs*elem_size) > BYTES_ACCESSED_TIMER) {
      val_accs = 0ull;
      clock_gettime(CLOCK_REALTIME, &thread_cur_time);
      elapsed_time = get_elapsed_seconds(thread_start_time, thread_cur_time);
    }
  }

  return NULL;
}

int main(int argc, char **argv)
{
  char *cold_arr, *hot_arr, **addr_arr;
  ssize_t elem_log2_size, cold_log2_elems, hot_log2_elems, addr_log2_elems,
          nr_threads, pos, i;
  double cold_pct, elems_per_sec, GB_per_sec, total_sec;
  char pattern;
  unsigned int seed, exe_time, default_device, hot_device, cold_device;
  unsigned long long cold_elems, hot_elems, addr_elems, elem_size,
                     hot_elem_cnt, cold_elem_cnt, ept, rem, total_elems;

  struct timespec main_start_time, main_finish_time;
  void *(*thread_routine)(void*);

  pthread_t *threads;
  struct thread_data_t *threads_data;

  sicm_device_list devs;
  sicm_arena default_arena, hot_arena, cold_arena;

  seed = 0;
  exe_time = 20;
  cold_pct = 50.0;
  nr_threads = 1;
  pattern = 'r';
  default_device = 0;

  elem_log2_size = cold_log2_elems = hot_log2_elems = addr_log2_elems = 0;
  parse_options(argc, argv, &elem_log2_size, &cold_log2_elems, &hot_log2_elems,
                &addr_log2_elems, &nr_threads, &cold_pct, &pattern, &seed,
                &exe_time, &default_device, &hot_device, &cold_device);


  if (pattern == 'r') {
    thread_routine = read_elems;
  } else if (pattern == 'w') {
    thread_routine = rdwr_elems;
  } else {
    printf("error: bad pattern: %c\n", pattern);
    exit(-EINVAL);
  }

  addr_elems = (1ull << addr_log2_elems);
  if ((addr_elems < nr_threads) ) {
    printf("error: addr_elems < nr_threads\n");
    printf(usage);
    exit(-EINVAL);
  }


	devs = sicm_init();
  default_arena = create_numa_sicm_arena(default_device, devs);
  if (hot_device != default_device) {
    hot_arena = create_numa_sicm_arena(hot_device, devs);
  } else {
    hot_arena = default_arena;
  }
  if ((cold_device != default_device) && (cold_device != hot_device)) {
    cold_arena = create_numa_sicm_arena(cold_device, devs);
  } else {
    cold_arena = (cold_device == default_device) ? default_arena : hot_arena;
  }


  cold_elems = (1ull << cold_log2_elems);
  hot_elems  = (1ull << hot_log2_elems);
  elem_size  = (1ull << elem_log2_size);
  srand(seed);

  hot_elem_cnt = cold_elem_cnt = 0;
  cold_arr = get_elem_array(cold_elems, elem_size, cold_arena);
  hot_arr  = get_elem_array(hot_elems, elem_size, hot_arena);
  addr_arr = get_addr_array(addr_elems, elem_size, cold_pct, hot_arr, cold_arr,
                            &hot_elem_cnt, &cold_elem_cnt, default_arena);
  shuffle_addr_array(addr_arr, addr_elems);

  // elements per thread
  ept = (addr_elems / nr_threads);
  rem = (addr_elems % nr_threads);

  threads = (pthread_t*) sicm_arena_alloc(default_arena,
                         (nr_threads*sizeof(pthread_t)));
  if (!threads) {
    exit(-ENOMEM);
  }

  threads_data = (struct thread_data_t*) sicm_arena_alloc(default_arena,
                                         nr_threads*sizeof(struct thread_data_t));
  if (!threads_data) {
    exit(-ENOMEM);
  }

  pos = 0;
  for (i = 0; i < nr_threads; i++) {
    threads_data[i].start_idx = pos;
    threads_data[i].nr_elems = (size_t)ept;
    threads_data[i].elem_size = elem_size;
    threads_data[i].addr_arr = addr_arr;
    threads_data[i].sum = 0ull;
    threads_data[i].nr_accs = 0ull;
    threads_data[i].exe_time = exe_time;

    pos += (size_t)ept;
    if (rem) {
      threads_data[i].nr_elems++;
      pos++;
      rem--;
    }
  }

  printf("membench configuration:\n");
  printf("%-24s:%20llu\n", "ELEMENT SIZE (B) ", elem_size);
  printf("%-24s:%20llu\n", "COLD ELEMENTS ", cold_elem_cnt);
  printf("%-24s:%20llu\n", "HOT ELEMENTS ", hot_elem_cnt);
  printf("%-24s:%20llu\n", "TOTAL ELEMENTS ", (hot_elem_cnt+cold_elem_cnt));
  printf("%-24s:%20lf\n", "COLD PORTION ",
      ((double)cold_elem_cnt / (hot_elem_cnt+cold_elem_cnt)));
  printf("%-24s:%20lu\n", "NUMBER THREADS ", nr_threads);
  printf("%-24s:%20s\n", "PATTERN ", (pattern == 'r' ? "READ" : "RD/WR"));
  printf("%-24s:%20d\n", "SEED ", seed);

  printf("\nRunning %lu threads for %u seconds ... ", nr_threads, exe_time);
  fflush(stdout);

  clock_gettime(CLOCK_REALTIME, &main_start_time);
  for (i = 0; i < nr_threads; i++) {
    pthread_create(&(threads[i]), NULL, thread_routine, &(threads_data[i]));
  }

  // wait on the threads to finish
  for (i = 0; i < nr_threads; i++) {
    pthread_join(threads[i], NULL);
  }
  clock_gettime(CLOCK_REALTIME, &main_finish_time);
  printf("done.\n\n"); fflush(stdout);

  total_elems = 0;
  for (i = 0; i < nr_threads; i++) {
    total_elems += threads_data[i].nr_accs;
  }

  total_sec = get_elapsed_seconds(main_start_time, main_finish_time);
  elems_per_sec = (double) total_elems / total_sec;
  GB_per_sec    = ( ( (((double) total_elems) * elem_size) /
                      total_sec
                    ) / ((double) (1ull<<30)) );
  printf("%-24s:%20.3f\n", "measured exe time (s)", total_sec);
  printf("%-24s:%20.3f\n", "elems/sec:", elems_per_sec);
  printf("%-24s:%20.3f\n", "GB/sec:", GB_per_sec);


  /* SICM cleanup */
  sicm_free(cold_arr);
  sicm_free(hot_arr);
  sicm_free(addr_arr);
  sicm_free(threads);
  sicm_free(threads_data);

  sicm_arena_destroy(default_arena);
  if (hot_arena != default_arena) {
    sicm_arena_destroy(hot_arena);
  }
  if ((cold_arena != default_arena) && (cold_arena != hot_arena)) {
    sicm_arena_destroy(cold_arena);
  }
	sicm_fini();

  return 0;
}
