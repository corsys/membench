# membench

membench is a memory access benchmark. The workload accesses data allocated in
two distinct arrays, repeatedly, and in parallel, using a specified number of
threads.

## Build

In the Makefile in the src directory, you will need to update the LIB_DIR and
INCLUDE_DIR variables to point to the lib and include directories for the SICM
low-level interface. The LIB_DIR should have the file libsicm.so.

Then, just do:

> cd src;
> make

These commands should build the membench executable, which you can use as
described below.


## Description

membench creates two arrays (hot and cold) with data elements. Each data
element is an array of some (configurable) number of bytes initialized with
random data. membench also creates a third array, called the address array,
which stores addresses to a subset of elements in the hot and cold arrays.
A command line option (cold_pct or -p) controls the portion of data elements
that are selected from the cold array, and all other elements are selected
from the hot array.

After initialization, membench shuffles the address array and divides it
evenly amongst a number of threads. The threads then continually traverse
their own portions of the address array in parallel and for some specified
amount of time. During this traversal, each thread sequentially accesses
the bytes in each data element in its portion of the address array with the
specified access pattern (read-only or read/write). The benchmark reports
throughput in both number of data elements accessed per second and GBs
accessed per second.

## Usage

**Command line arguments**
Argument           Description
--elem_size,-E     Required, size of elements in data arrays as power of 2
--cold_elems,-C    Required, size of cold array as power of 2
--hot_elems,-H     Required, size of hot array as power of 2
--addr_elems,-A    Size of address array as power of 2, defaults to min size of hot/cold arrays
--threads,-t       Number of threads to run workload, defaults to 1
--cold_pct,-p      Percent chance of selecting an address from the cold array, defaults to 50%
--pattern,-r       Access pattern, read-only : 'r' or read-write : 'w', defaults to read-only
--seed,-s          Value to seed random number generator with, defaults to 0
--exe_time,-x      Time in seconds for threads to traverse the address array, defaults to 20
--default_dev,-d   Default NUMA node for heap data, defaults to 0
--hot_dev,-m       NUMA node for hot array, defaults to default_dev
--cold_dev,-n      NUMA node for cold array, defaults to default_dev

Restrictions
elem_size must be between 3 and 63
cold_elems and hot_elems must between 0 and 63
addr_elems must be at least log2(threads), and cannot be larger than cold_elems or hot_elems
threads must be between 1 and sysconf(_SC_NPROCESSORS_ONLN)
cold_pct must be between 0 and 100

### Example
Run benchmark with 64KB elements, 64K (2^16) elements in the cold array,
64K elements in the hot array, 64K elements in the address array. Splits
workload among 6 threads. About 20% of the elements in the address array
correspond to data elements in the cold array. Access pattern is read-only.
The threads access elements in the address array for at least 20 seconds. 

All threads are bound to numa node 1 using numactl. The last three options
(-d, -m, and -n) specify that the hot array and other heap data are bound to
numa node 1, and the cold array is bound to numa node 0 using the SICM
low-level interface.

numactl --cpunodebind=1 ./membench -E 16 -C 16 -H 16 -A 16 -t 6 -p 20.0 -r r -x 20 -d 1 -m 1 -n 0

Output:

Node count: 2
membench configuration:
ELEMENT SIZE (B)        :               65536
COLD ELEMENTS           :               13089
HOT ELEMENTS            :               52447
TOTAL ELEMENTS          :               65536
COLD PORTION            :            0.199722
NUMBER THREADS          :                   6
PATTERN                 :                READ
SEED                    :                   0

Running 6 threads for 20 seconds ... done.

measured exe time (s)   :              20.233
elems/sec:              :          412440.116
GB/sec:                 :              25.173


Note if we reduce the number of cold data elements in the address array,
throughput improves because a higher proportion of accesses will be resolved on
the local numa node. For example, the run:

numactl --cpunodebind=1 ./membench -E 16 -C 16 -H 16 -A 16 -t 6 -p 5.0 -r r -x 20 -d 1 -m 1 -n 0

Outputs:

Node count: 2
membench configuration:
ELEMENT SIZE (B)        :               65536
COLD ELEMENTS           :                3239
HOT ELEMENTS            :               62297
TOTAL ELEMENTS          :               65536
COLD PORTION            :            0.049423
NUMBER THREADS          :                   6
PATTERN                 :                READ
SEED                    :                   0

Running 6 threads for 20 seconds ... done.

measured exe time (s)   :              20.169
elems/sec:              :          648794.440
GB/sec:                 :              39.599

